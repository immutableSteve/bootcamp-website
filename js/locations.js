const offices = [
  { coor: { lon: 4.899431, lat: 52.379189 }, city: "Amsterdam" },
  { coor: { lon: 3.722, lat: 51.049 }, city: "Ghent" },
  { coor: { lon: 5.472, lat: 51.42 }, city: "Eindhoven" },
  { coor: { lon: 13.378, lat: 52.51 }, city: "Berlin" },
  { coor: { lon: 19.437, lat: 51.759 }, city: "Lodz" },
  { coor: { lon: 16.903, lat: 52.397 }, city: "Poznan" },
  { coor: { lon: -3.201, lat: 55.954 }, city: "Edinburgh" }
];

var map = tt.map({
  key: "ciedzdzS4B0erkNBH1hKhj67fbyqQyK3",
  container: "map",
  style: "tomtom://vector/1/basic-main",
  center: [10, 53],
  zoom: 5,
  interactive: false
});

function createMarkerElement(city) {
  let element = document.createElement("div");
  element.innerHTML = `<a class="city default" href="gent.html">
  <span class="white">  
  <i class="fas fa-building office"></i>
    ${city} </span>
    </a>`;
  /*'<img id="car" src="../images/car.png" style="transform:rotate(' +
    degrees +
    'deg);"/>';*/
  return element;
}

function createTTMarker(office) {
  return new tt.Marker({ element: createMarkerElement(office.city) }).setLngLat(
    office.coor
  );
}

offices.forEach(office => {
  var marker = createTTMarker(office);
  marker.addTo(map);
});
