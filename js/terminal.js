var cursor = $("#cursor");
var setter = $("#setter");
var writer = $("#writer");
var terminal = $("#terminal");
var question;

function selectQuestion() {
  let random = Math.floor(Math.random() * questions.length);
  question = questions[random];
  document.getElementById("question").innerText = question.q;
}
selectQuestion();

$(cursor).css("left", "0px");
function nl2br(txt) {
  return txt.replace(/\n/g, "<br />guest@tomtom.careers.com:~$- ");
}

function getCurrentText() {
  let terminalText = $(setter).val().split("\n");
  return terminalText[terminalText.length - 1];
}

function checkKeyDown(e) {
  let currentText = getCurrentText();
  if (event.keyCode === 13) {
    validateResponse(e, currentText);
  }
  disableDeleteOnLastCharacter(e, currentText);
}

function challengeAccepted() {
  document.querySelector("#question-challenge").style.display = "block";
  document.getElementById('setter').focus();
}


function validateResponse(e, currentText) {
  if (currentText.toLowerCase().trim() === question.r) {
    document.getElementById("writer").innerText +=
    "\nrecruiter@tomtom.careers.com:~$ Well done, you have unlock new content";
    $(setter).off();
    document.getElementById("secret-video").style.display = "block";
  }
  if (currentText.toLowerCase().trim() === "tomtom") {
    document.getElementById("writer").innerText +=
    "\nrecruiter@tomtom.careers.com:~$ (¯`·._.· Hail to TomTom ·._.·´¯)";
    $(setter).off();
    document.getElementById("secret-video").style.display = "block";
  }
}

function disableDeleteOnLastCharacter(e, currentText) {
  e = e || event;
  let code = e.which || e.keyCode || e.charCode;
  let cando = !([8, 46].indexOf(code) > -1 && currentText.length <= 0);
  console.log(cando);
  if(!cando) {
    event.preventDefault();
    event.stopPropagation();
  }
}

function writeit(from, e) {
  e = e || window.event;
  var w = $(writer);
  var tw = from.value;
  w.html(nl2br(tw));
}

function moveIt(count, e) {
  e = e || window.event;
  var keycode = e.keyCode || e.which;
  if (
    keycode == 37 &&
    parseInt($(cursor).css("left")) >= 0 - (count - 1) * 10
  ) {
    $(cursor).css("left", parseInt($(cursor).css("left")) - 10 + "px");
  } else if (keycode == 39 && parseInt($(cursor).css("left")) + 10 <= 0) {
    $(cursor).css("left", parseInt($(cursor).css("left")) + 10 + "px");
  }
}
function blink() {
  if ($(cursor).css("display") == "none") {
    $(cursor).css("display", "inline");
  } else {
    $(cursor).css("display", "none");
  }
}
setInterval("blink()", 500);
$(terminal).click(function() {
  $(setter).focus();
});

function setListenersOnTerminal() {
  $(setter).keydown(function(event) {
    writeit(this, event);
    checkKeyDown(event);
    moveIt(this.value.length, event);
  });
  $(setter).keyup(function(event) {
    writeit(this, event);
  });
  $(setter).keypress(function(event) {
    writeit(this, event);
  });
}
setListenersOnTerminal();